import click
from .config import CONFIG

@click.command()
@click.option('--item', required=True, help='Item to add.')
def add_item(item):
    """Add a new item."""
    try: 
        # Do the work
        # log the success
        click.echo(f'{item} added.')
    catch:
        # log the error
        click.echo(f'{item} failed to add.')

        
if __name__ == '__main__':
    add_item()
  