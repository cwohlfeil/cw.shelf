from datetime import datetime
from typing import Set, Dict
from tinydb import TinyDB, Query

from .config import CONFIG

db = TinyDB(CONFIG.json_db_name)

def insert_entry(string: name, string: resource_location, Set[string]: tags) -> bool:
    try:
        db.insert({'name': name, 
                   'resource_location': resource_location, 
                   'datetime_added': datetime.utcnow(),
                   'archived': False,
                   'tags': []})
        return True
    else:
        return False
    

def get_entry_by_name(string: name) -> Dict:
    Entry = Query()
    try:
        return db.search(Entry.name == name)
    except IndexError:
        pass
    
    
def get_all_entries():
    