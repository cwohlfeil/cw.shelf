from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from .config import CONFIG

# Create and declare SQLAlchemy engine
engine = create_engine(f'sqlite:///{CONFIG.sqlite_db_name}', echo=True)
Base = declarative_base(bind=engine)
Session = scoped_session(sessionmaker(engine))


class Entry(Base):
    """
    Entry DB model to represent books, ebooks, and links.
    resource_location: either a link or reference to the location, i.e. "Kindle", "Physical"
    """
    __tablename__ = 'Entry'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    resource_location = Column(String)
    archived = Column(Boolean)
    
    def __repr__(self):
        return f"<Entry(name='{self.name}', resource_location='{self.resource_location}', archived='{self.archived}')>"

    
class Tag(Base):
    """
    Tag DB model to represent tags.
    """
    __tablename__ = 'Tag'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    

class EntryToTag(Base):
    """
    Entry to Tag DB model to associate entries with tags.
    """
    __tablename__ = 'Entry_to_Tag'

    entry_id = Column(Integer, ForeignKey("Entry.id"), nullable=False)
    tag_id = Column(Integer, ForeignKey("Tag.id"), nullable=False)
      
    
    
Base.metadata.create_all()